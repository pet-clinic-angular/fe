import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css'],
})
export class ToolbarComponent implements OnInit, AfterViewInit {
  @ViewChild('home', { read: ElementRef }) home!: ElementRef;
  @ViewChild('owners', { read: ElementRef }) owners!: ElementRef;
  @ViewChild('vets', { read: ElementRef }) vets!: ElementRef;
  @ViewChild('error', { read: ElementRef }) error!: ElementRef;

  private buttonColor: string = 'rgb(52, 48, 45)';
  private buttonClickedColor: string = 'rgb(109, 179, 63)';
  language!: string;

  constructor(private router: Router) {}

  ngAfterViewInit(): void {
    let buttonClicked: string = sessionStorage.getItem('button') || '';

    switch (buttonClicked) {
      case 'home':
        this.homeButtonClickedColor();
        break;
      case 'owners':
        this.ownersButtonClickedColor();
        break;
      case 'vets':
        this.vetsButtonClickedColor();
        break;
      case 'error':
        this.errorButtonClickedColor();
        break;
      default:
        this.homeButtonClickedColor();
        break;
    }
  }

  ngOnInit(): void {
    this.language = sessionStorage.getItem('lang') || 'ro';
  }

  goToHome(): void {
    sessionStorage.setItem('button', 'home');
    this.homeButtonClickedColor();
    this.router.navigate(['home']);
  }

  goToOwners(): void {
    sessionStorage.setItem('button', 'owners');
    this.ownersButtonClickedColor();
    this.router.navigate(['owners/table']);
  }

  goToVets(): void {
    sessionStorage.setItem('button', 'vets');
    this.vetsButtonClickedColor();
    this.router.navigate(['vets/table']);
  }

  goToError(): void {
    sessionStorage.setItem('button', 'error');
    this.errorButtonClickedColor();
    this.router.navigate(['error']);
  }

  changeLanguageToRomanian(): void {
    sessionStorage.setItem('lang', 'en');
    this.language = sessionStorage.getItem('lang') || 'ro';
    window.location.reload();
  }

  changeLanguageToEnglish(): void {
    sessionStorage.setItem('lang', 'ro');
    this.language = sessionStorage.getItem('lang') || 'ro';
    window.location.reload();
  }

  private homeButtonClickedColor(): void {
    this.home.nativeElement.style.backgroundColor = this.buttonClickedColor;
    this.owners.nativeElement.style.backgroundColor = this.buttonColor;
    this.vets.nativeElement.style.backgroundColor = this.buttonColor;
    this.error.nativeElement.style.backgroundColor = this.buttonColor;
  }

  private ownersButtonClickedColor(): void {
    this.home.nativeElement.style.backgroundColor = this.buttonColor;
    this.owners.nativeElement.style.backgroundColor = this.buttonClickedColor;
    this.vets.nativeElement.style.backgroundColor = this.buttonColor;
    this.error.nativeElement.style.backgroundColor = this.buttonColor;
  }

  private vetsButtonClickedColor(): void {
    this.home.nativeElement.style.backgroundColor = this.buttonColor;
    this.owners.nativeElement.style.backgroundColor = this.buttonColor;
    this.vets.nativeElement.style.backgroundColor = this.buttonClickedColor;
    this.error.nativeElement.style.backgroundColor = this.buttonColor;
  }

  private errorButtonClickedColor(): void {
    this.home.nativeElement.style.backgroundColor = this.buttonColor;
    this.owners.nativeElement.style.backgroundColor = this.buttonColor;
    this.vets.nativeElement.style.backgroundColor = this.buttonColor;
    this.error.nativeElement.style.backgroundColor = this.buttonClickedColor;
  }
}
