import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './componets/home-page/home-page.component';
import { VetComponent } from './module/vet/vet-table/vet.component';
import { OwnerComponent } from './module/owner/owners-table/owner.component';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ToolbarComponent } from './componets/toolbar/toolbar.component';
import { ErrorPageComponent } from './componets/error-page/error-page.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MatSortModule } from '@angular/material/sort';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { OwnerInfoComponent } from './module/owner/owner-info/owner-info.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { InactiveOwnersTableComponent } from './module/owner/inactive-owners-table/inactive-owners-table.component';
import { MakeOwnerInactiveDialogComponent } from './module/owner/owners-table/make-owner-inactive-dialog/make-owner-inactive-dialog.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from './utils/httpLoaderFactory';
import { VetInfoComponent } from './module/vet/vet-info/vet-info.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    VetComponent,
    OwnerComponent,
    ToolbarComponent,
    ErrorPageComponent,
    OwnerInfoComponent,
    InactiveOwnersTableComponent,
    MakeOwnerInactiveDialogComponent,
    VetInfoComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    MatTableModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatSortModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [HttpClient],
  bootstrap: [AppComponent],
})
export class AppModule {}
