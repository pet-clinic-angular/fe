import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorPageComponent } from './componets/error-page/error-page.component';
import { HomePageComponent } from './componets/home-page/home-page.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: HomePageComponent },
  {
    path: 'vets',
    loadChildren: () =>
      import('./module/vet/vet.module').then((m) => m.VetModule),
  },
  {
    path: 'owners',
    loadChildren: () =>
      import('./module/owner/owner.module').then((m) => m.OwnerModule),
  },
  { path: 'error', component: ErrorPageComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
