import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Owner } from '../model/owner';

@Injectable({
  providedIn: 'root',
})
export class OwnerService {
  private url = 'http://localhost:8080/api';

  constructor(private http: HttpClient) {}

  getActiveOwners(): Observable<Owner[]> {
    return this.http.get<Owner[]>(`${this.url}/owners`);
  }

  getInactiveOwners(): Observable<Owner[]> {
    return this.http.get<Owner[]>(`${this.url}/owners/inactive`);
  }

  changeOwnerIsActiveFlag(id: number): void {
    const body = id;
    this.http.patch(`${this.url}/change-flag/${id}`, body).subscribe(() => {});
  }
}
