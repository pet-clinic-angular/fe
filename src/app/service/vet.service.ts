import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Vet } from '../model/vet';
import { VetDescription } from '../model/vetDescription';

@Injectable({
  providedIn: 'root',
})
export class VetService {
  private url = 'http://localhost:8080/api';

  constructor(private http: HttpClient) {}

  getVets(): Observable<Vet[]> {
    return this.http.get<Vet[]>(`${this.url}/vets`);
  }

  getVetDescription(
    vetId: number,
    language: string
  ): Observable<VetDescription> {
    return this.http.get<VetDescription>(
      `${this.url}/vet/${vetId}/${language}`
    );
  }
}
