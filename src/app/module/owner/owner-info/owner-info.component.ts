import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Owner } from 'src/app/model/owner';
import { Pet } from 'src/app/model/pet';
import { Visit } from 'src/app/model/visit';

@Component({
  selector: 'app-owner-info',
  templateUrl: './owner-info.component.html',
  styleUrls: ['./owner-info.component.css'],
})
export class OwnerInfoComponent implements OnInit {
  ownerId: number = Number(this.route.snapshot.paramMap.get('ownerId'));
  owner!: Owner;
  pets!: Pet[];
  visits: Visit[] = [];
  language!: string;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.language = sessionStorage.getItem('lang') || 'ro';
    this.getOwner();
    this.getOwnersPets();
    this.getPetVisits();
  }

  getOwner(): void {
    let owners: Owner[] = JSON.parse(
      sessionStorage.getItem('active-owners') || ''
    );
    for (let index = 0; index < owners.length; index++) {
      if (this.ownerId === owners[index].id) {
        this.owner = owners[index];
        break;
      }
    }
  }

  getOwnersPets(): void {
    this.pets = this.owner.pets;
  }

  getPetVisits(): void {
    this.pets.forEach((pet) => {
      for (let index = 0; index < pet.visits.length; index++) {
        this.visits.push(pet.visits[index]);
      }
    });
  }

  goToOwnersTable(): void {
    this.router.navigate(['owners/table']);
  }
}
