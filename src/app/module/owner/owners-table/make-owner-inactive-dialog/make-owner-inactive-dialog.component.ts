import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OwnerService } from 'src/app/service/owner.service';

@Component({
  selector: 'app-make-owner-inactive-dialog',
  templateUrl: './make-owner-inactive-dialog.component.html',
  styleUrls: ['./make-owner-inactive-dialog.component.css'],
})
export class MakeOwnerInactiveDialogComponent implements OnInit {
  ownerName!: string;
  ownerId!: number;

  constructor(
    public dialogRef: MatDialogRef<MakeOwnerInactiveDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ownerService: OwnerService
  ) {}

  ngOnInit(): void {
    this.ownerName = this.data.ownerFirstName + ' ' + this.data.ownerLastName;
    this.ownerId = this.data.ownerId;
  }

  changeOwnerToInactive() {
    this.ownerService.changeOwnerIsActiveFlag(this.ownerId);
    sessionStorage.removeItem('active-owners');
    window.location.reload();
  }
}
