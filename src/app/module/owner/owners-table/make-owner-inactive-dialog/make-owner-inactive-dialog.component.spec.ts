import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MakeOwnerInactiveDialogComponent } from './make-owner-inactive-dialog.component';

describe('MakeOwnerInactiveDialogComponent', () => {
  let component: MakeOwnerInactiveDialogComponent;
  let fixture: ComponentFixture<MakeOwnerInactiveDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MakeOwnerInactiveDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MakeOwnerInactiveDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
