import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Owner } from 'src/app/model/owner';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { OwnerService } from 'src/app/service/owner.service';
import { lastValueFrom } from 'rxjs';
import { MakeOwnerInactiveDialogComponent } from './make-owner-inactive-dialog/make-owner-inactive-dialog.component';

@Component({
  selector: 'app-owner',
  templateUrl: './owner.component.html',
  styleUrls: ['./owner.component.css'],
})
export class OwnerComponent implements OnInit {
  displayedColumns: string[] = ['number', 'firstName', 'lastName', 'actions'];
  dataSource = new MatTableDataSource<Owner>();
  isDataSource!: boolean;
  noOwnersFoundError: string = '';
  owners!: Owner[];
  inProgress!: boolean;

  constructor(
    private ownerService: OwnerService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.displayOwners();
  }

  async displayOwners() {
    try {
      if (!sessionStorage.getItem('active-owners')) {
        this.inProgress = true;
        const owners$ = this.ownerService.getActiveOwners();
        this.owners = await lastValueFrom(owners$);
        sessionStorage.setItem('active-owners', JSON.stringify(this.owners));
      }
      this.inProgress = false;
      this.addOwnersToTable();
    } catch (err) {
      this.inProgress = false;
      this.isDataSource = false;
      this.noOwnersFoundError = 'No Owners found!';
    }
  }

  addOwnersToTable(): void {
    this.isDataSource = true;
    this.dataSource.data = JSON.parse(
      sessionStorage.getItem('active-owners') || ''
    );
  }

  doFilter(event: Event) {
    this.dataSource.filter = (event.target as HTMLInputElement).value
      .trim()
      .toLocaleLowerCase();
  }

  navigateToInfoPage(ownerId: number) {
    this.router.navigate(['owners/info', { ownerId: ownerId }]);
  }

  openDialog(
    ownerFirstName: string,
    ownerLastName: string,
    ownerId: number
  ): void {
    this.dialog.open(MakeOwnerInactiveDialogComponent, {
      width: '330px',
      height: '200px',
      data: {
        ownerFirstName,
        ownerLastName,
        ownerId,
      },
    });
  }

  goToInactiveOwnersTable(): void {
    sessionStorage.removeItem('inactive-owners');
    this.router.navigate(['owners/inactive-table']);
  }
}
