import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InactiveOwnersTableComponent } from './inactive-owners-table/inactive-owners-table.component';
import { OwnerInfoComponent } from './owner-info/owner-info.component';
import { OwnerComponent } from './owners-table/owner.component';

const routes: Routes = [
  { path: 'table', component: OwnerComponent },
  { path: 'info', component: OwnerInfoComponent },
  { path: 'inactive-table', component: InactiveOwnersTableComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OwnerRoutingModule {}
