import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InactiveOwnersTableComponent } from './inactive-owners-table.component';

describe('InactiveOwnersTableComponent', () => {
  let component: InactiveOwnersTableComponent;
  let fixture: ComponentFixture<InactiveOwnersTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InactiveOwnersTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InactiveOwnersTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
