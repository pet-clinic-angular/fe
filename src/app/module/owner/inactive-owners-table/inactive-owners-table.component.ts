import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { Owner } from 'src/app/model/owner';
import { OwnerService } from 'src/app/service/owner.service';

@Component({
  selector: 'app-inactive-owners-table',
  templateUrl: './inactive-owners-table.component.html',
  styleUrls: ['./inactive-owners-table.component.css'],
})
export class InactiveOwnersTableComponent implements OnInit {
  displayedColumns: string[] = ['number', 'firstName', 'lastName', 'actions'];
  dataSource = new MatTableDataSource<Owner>();
  isDataSource!: boolean;
  noInactiveOwnersFoundError: string = '';
  inactiveOwners!: Owner[];
  inProgress!: boolean;

  constructor(private ownerService: OwnerService, private router: Router) {}

  ngOnInit(): void {
    this.displayInactiveOwners();
  }

  async displayInactiveOwners() {
    try {
      if (!sessionStorage.getItem('inactive-owners')) {
        this.inProgress = true;
        const owners$ = this.ownerService.getInactiveOwners();
        this.inactiveOwners = await lastValueFrom(owners$);
        sessionStorage.setItem(
          'inactive-owners',
          JSON.stringify(this.inactiveOwners)
        );
      }
      this.inProgress = false;
      this.addOwnersToTable();
    } catch (err) {
      this.inProgress = false;
      this.isDataSource = false;
      this.noInactiveOwnersFoundError = 'No Owners found!';
    }
  }

  addOwnersToTable(): void {
    this.isDataSource = true;
    this.dataSource.data = JSON.parse(
      sessionStorage.getItem('inactive-owners') || ''
    );
  }

  doFilter(event: Event) {
    this.dataSource.filter = (event.target as HTMLInputElement).value
      .trim()
      .toLocaleLowerCase();
  }

  goToOwnersTable(): void {
    sessionStorage.removeItem('active-owners');
    this.router.navigate(['owners/table']);
  }

  changeOwnerToActive(ownerId: number): void {
    this.ownerService.changeOwnerIsActiveFlag(ownerId);
    sessionStorage.removeItem('inactive-owners');
    window.location.reload();
  }
}
