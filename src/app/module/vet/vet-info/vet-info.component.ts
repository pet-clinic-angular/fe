import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Vet } from 'src/app/model/vet';
import { VetDescription } from 'src/app/model/vetDescription';
import { VetService } from 'src/app/service/vet.service';

@Component({
  selector: 'app-vet-info',
  templateUrl: './vet-info.component.html',
  styleUrls: ['./vet-info.component.css'],
})
export class VetInfoComponent implements OnInit {
  vetId: number = Number(this.route.snapshot.paramMap.get('vetId'));
  vet!: Vet;
  language: string = '';
  vetDescription: string = '';
  noVetDescriptionError: string = '';
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private vetService: VetService
  ) {}

  ngOnInit(): void {
    this.language = sessionStorage.getItem('lang') || 'ro';
    this.getVet();
    this.getVetDescription();
  }

  getVet(): void {
    let vets: Vet[] = JSON.parse(sessionStorage.getItem('vets') || '');
    for (let index = 0; index < vets.length; index++) {
      if (vets[index].id === this.vetId) {
        this.vet = vets[index];
        break;
      }
    }
  }

  getVetDescription(): void {
    this.vetService
      .getVetDescription(this.vetId, this.language)
      .subscribe((data: VetDescription) => {
        if (data) {
          this.vetDescription = data.description;
        } else {
          this.noVetDescriptionError = 'No Vet Description Found!';
        }
      });
  }

  goToVetsTable(): void {
    this.router.navigate(['vets/table']);
  }
}
