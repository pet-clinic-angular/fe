import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { Vet } from 'src/app/model/vet';
import { VetService } from 'src/app/service/vet.service';

@Component({
  selector: 'app-vet',
  templateUrl: './vet.component.html',
  styleUrls: ['./vet.component.css'],
})
export class VetComponent implements OnInit {
  displayedColumns: string[] = ['number', 'firstName', 'lastName', 'actions'];
  dataSource = new MatTableDataSource<Vet>();
  isDataSource!: boolean;
  noVetsFoundError: string = '';
  vets!: Vet[];
  inProgress!: boolean;

  constructor(private vetService: VetService, private router: Router) {}

  ngOnInit(): void {
    this.displayVets();
  }

  async displayVets() {
    try {
      if (!sessionStorage.getItem('vets')) {
        this.inProgress = true;
        const vets$ = this.vetService.getVets();
        this.vets = await lastValueFrom(vets$);
        sessionStorage.setItem('vets', JSON.stringify(this.vets));
      }
      this.inProgress = false;
      this.addVetsToTable();
    } catch (err) {
      this.inProgress = false;
      this.isDataSource = false;
      this.noVetsFoundError = 'No Vets found!';
    }
  }

  addVetsToTable(): void {
    this.isDataSource = true;
    this.dataSource.data = JSON.parse(sessionStorage.getItem('vets') || '');
  }

  doFilter(event: Event) {
    this.dataSource.filter = (event.target as HTMLInputElement).value
      .trim()
      .toLocaleLowerCase();
  }

  navigateToInfoPage(vetId: number) {
    this.router.navigate(['vets/info', { vetId: vetId }]);
  }
}
