import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VetInfoComponent } from './vet-info/vet-info.component';
import { VetComponent } from './vet-table/vet.component';

const routes: Routes = [
  { path: 'table', component: VetComponent },
  { path: 'info', component: VetInfoComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VetRoutingModule {}
