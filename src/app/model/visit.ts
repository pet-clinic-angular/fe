export interface Visit {
  id: number;
  date: string;
  description: string;
}
