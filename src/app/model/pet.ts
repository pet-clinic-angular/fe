import { PetType } from './petType';
import { Visit } from './visit';

export interface Pet {
  id: number;
  name: string;
  petType: PetType;
  birthDate: string;
  visits: Visit[];
}
